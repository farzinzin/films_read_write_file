<?php

/* Auteur: Farzin Faridfar
 * Cours: IFT1147 Programmation serveur Web avec PHP
 * Date: 05 nov 2016
 * Fichier: film.inc.php 
 * Desc: fichier de la classe Film, pour creer les instances
 * de type Film
 */

class Film
{
    private $numero;
    private $titre;
    private $realisateur;
    private $categorie;
    private $duree;
    private $prix;
    private $image;

    function __construct($numero, $titre, $realisateur,
                         $categorie, $duree, $prix, $image) 
    {
        $this->setNumero($numero);
        $this->setTitre($titre);
        $this->setRealisateur($realisateur);
        $this->setCategorie($categorie);
        $this->setDuree($duree);
        $this->setPrix($prix);
        $this->setImage($image);
    }

    function __destruct() {}

    function setNumero($numero)
    {
        // $patternNumero = '/[0-9]{2,}/';
        // if (preg_match($patternNumero,$numero)) 
            $this->numero = $numero;
        // else
        //     echo "Pourriez vous entrez un numero valid.";
    }
    
    function setTitre($titre) 
    {
        $this->titre = $titre;
    }

    function setRealisateur($realisateur) 
    {
        //$patternRealisateur = '/^[A-Za-z]+ ?[A-Za-z]+$/';
        //if (preg_match($patternRealisateur,$realisateur))
            $this->realisateur = $realisateur;
        //else
        //     echo "Pourriez vous entrez un nom valid.";
    }

    function setCategorie($categorie) 
    {
        $this->categorie = $categorie;
    }

    function setDuree($duree) 
    {
        // $patternDuree = '/^[1-9][0-9]{0,4}$/';
        // if (preg_match($patternDuree,$duree)) 
            $this->duree = $duree;         
            //else
            //echo "Pourriez vous entrez une duree valid en minuntes.";
            //echo $duree;      
    }

    function setPrix($prix) 
    {
        // $patternPrix ='/^[1-9][0-9]{1,2}.?[0-9]{1,2}$/';
        // if (preg_match($patternPrix,$prix)) 
            $this->prix = $prix;    
        // else
        //     echo "Pourriez vous entrez un prix valid en CAD.";
            //echo $prix."<br>";
    }

    //pour creer l'adress de l'image a partir
    //de son nom qui est enregistre dans un dossier
    function setImage($image) 
    {
        $this->image = $image;
    }

    function getNumero() 
    {
        return $this->numero;
    }

    function getTitre() 
    {
        return $this->titre;
    }

    function getRealisateur() 
    {
        return $this->realisateur;
    }

    function getCategorie() 
    {
        return $this->categorie;
    }

    function getDuree() 
    {
        return $this->duree;
    }

    function getPrix() 
    {
        return $this->prix;
    }

    function getImage() 
    {
        return $this->image;
    }

}

?>
