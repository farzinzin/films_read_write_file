<script language='javascript' src="js/monJS.js"></script>
<link rel="stylesheet" type="text/css" href="css/inputStyle.css">
<?php
/*
 * Auteur: Farzin Faridfar
 * Date: 05 nov 2016
 * Cours: IFT1147 Programmation serveur Web avec PHP
 * Fichier: gestionFilms.php 
 * Desc: gerer les requetes recue de la fichier accueil selon
 * leurs demandes qui incluent afficher list, enregistrer,
 * supprimer et modifier
 */

require_once("film.inc.php");//fichier de la classe du Film

$tabFilms=array();
$fichier=fopen("films.txt", "r");

if($fichier==null)
{
    echo "<br>Fichier Introuvable.\n";
    exit;
}

//$tabFilms[0] contient l'entete du fichier

$ligne=fgets($fichier);
while(!feof($fichier))
{
    $tmpTab=explode(";", $ligne);
    //tab[i] pour i [0 6] contient les att. d'une instance Film:
    //0:numero, 1:titre, 2:realisateur, 3:categorie
    //4:duree, 5:prix, 6:image
    $tabFilms[]=new Film($tmpTab[0],$tmpTab[1],$tmpTab[2],$tmpTab[3],
                         $tmpTab[4],$tmpTab[5],$tmpTab[6]);
    $ligne=fgets($fichier);
}
fclose($fichier);

function afficherTabFilms()
{
    global $tabFilms;
    $taille=count($tabFilms);
    trier();
    echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"css/tableStyle.css\">";
    echo "<table>";
    //l'entet du tableau
    echo "<tr>";
    echo "<th>".$tabFilms[0]->getNumero()."</th>";
    echo "<th>".$tabFilms[0]->getTitre()."</th>";
    echo "<th>".$tabFilms[0]->getRealisateur()."</th>";
    echo "<th>".$tabFilms[0]->getCategorie()."</th>"; 
    echo "<th>".$tabFilms[0]->getDuree()."</th>";
    echo "<th>".$tabFilms[0]->getPrix()."</th>";
    echo "<th>Pochette</th>";
    echo "</tr>";
    for($i=1; $i<$taille; ++$i)
    {
        echo "<tr>";
        echo "<td>".$tabFilms[$i]->getNumero()."</td>";
        echo "<td>".$tabFilms[$i]->getTitre()."</td>";
        echo "<td>".$tabFilms[$i]->getRealisateur()."</td>";
        echo "<td>".$tabFilms[$i]->getCategorie()."</td>";
        echo "<td>".$tabFilms[$i]->getDuree()." min</td>";
        echo "<td>".$tabFilms[$i]->getPrix()." $</td>"; 
        echo "<td><img src=\"images/image_"
            .$tabFilms[$i]->getImage().
            ".jpg\" style='width:65px;height:95px;'></td>"; 
        echo "</tr>";
    }
    echo "</table>";
}

function enregistrerFilmFichier()
{
    global $tabFilms;
    $taille=count($tabFilms);
    $ligne=$tabFilms[$taille-1]->getNumero().";".
          $tabFilms[$taille-1]->getTitre().";".
          $tabFilms[$taille-1]->getRealisateur().";".
          $tabFilms[$taille-1]->getCategorie().";".
          $tabFilms[$taille-1]->getDuree().";".
          $tabFilms[$taille-1]->getPrix().";".
          $tabFilms[$taille-1]->getImage()."\n";
    $sortie=fopen("films.txt","a");
    fputs($sortie,$ligne);
    fclose($sortie);
}


function trier()
{
    global $tabFilms;
    $taille=count($tabFilms);
    for($i=1 ;$i<$taille-1 ;++$i)
        for($j=($i+1); $j<$taille ;$j++)
            if ($tabFilms[$j]->getNumero() <
                $tabFilms[$i]->getNumero()){
                $tmp=$tabFilms[$i];
                $tabFilms[$i]=$tabFilms[$j];
                $tabFilms[$j]=$tmp;
            }
}


function enregistrer()
{
    global $tabFilms;
    $numero=$_POST['numeroE'];
    $titre=$_POST['titre'];
    $realisateur=$_POST['realisateur'];
    $categorie=$_POST['categorie'];
    $duree=$_POST['duree'];
    $prix=$_POST['prix'];
    $image=$_POST['image'];
    $tabFilms[]=new Film($numero,$titre,$realisateur,$categorie,
                         $duree,$prix,$image);
    enregistrerFilmFichier();
}

function supprimer($numero)
{
    global $tabFilms;
    $taille=count($tabFilms);
    
    $sortie=fopen("films.txt","w");
    fputs($sortie,$ligne);

    for($i=0;$i<$taille;++$i)
    {
        if($tabFilms[$i]->getNumero() !== $numero)
        {
            $ligne=$tabFilms[$i]->getNumero().";".
                  $tabFilms[$i]->getTitre().";".
                  $tabFilms[$i]->getRealisateur().";".
                  $tabFilms[$i]->getCategorie().";".
                  $tabFilms[$i]->getDuree().";".
                  $tabFilms[$i]->getPrix().";".
                  $tabFilms[$i]->getImage();
            fputs($sortie,$ligne);
        }
    }
    fclose($sortie);
}

function rechercher()
{
    global $tabFilms;
    $taille=count($tabFilms);
    $numero=trim($_POST['numeroM']);
    for($i=0; $i<$taille; ++$i)
    {
        if($tabFilms[$i]->getNumero()==$numero)
        {
            $film = $tabFilms[$i];
        }
    }
    
    //if($i<$taille)//film est touve dans la taille de $tabFilms
    return $film;
    //else //film n'est pas trouve a l'interieur du $tabFilms
        //return null;
}

function envoyerInfoEnreg()
{
    global $tabFilms;
    trier();
    $taille = count($tabFilms)-1;//-1 pour la premier cas qui a l'entet du tabFilms et pas un film
    echo "      <form name=\"formEnreg\" action=\"gestionFilms.php\" method='post'>\n"; 
    echo "	<div>\n"; 
    echo "	  Numéro:<br><input type='text' class=\"enregIn\" id=\"numeroE\" name=\"numeroE\" value=\"".($tabFilms[$taille]->getNumero()+1)."\" readonly></br>\n"; 
    echo "	  Tire:<br><input type='text'  class=\"enregIn\" id=\"titre\" name=\"titre\"></br>\n"; 
    echo "	  Réalisateur:<br><input type='text'  class=\"enregIn\" id=\"realisateur\" name=\"realisateur\"></br>\n"; 
    echo "	  Catégorie:<br>\n"; 
    echo "	  <select class=\"selEnreg\" id=\"categorie\" name=\"categorie\">\n"; 
    echo "	    <option name=\"action\">Action</option>\n"; 
    echo "	    <option name=\"dram_rep\">Drame et Répertoire</option>\n"; 
    echo "	    <option name=\"comedie\">Comédie</option>\n"; 
    echo "	    <option name=\"scifi\">Sience Fiction</option>\n"; 
    echo "	    <option name=\"horreur\">Horreur</option>\n"; 
    echo "	    <option name=\"suspense\">Suspense</option>\n"; 
    echo "	    <option name=\"musicale\">Comédies Musicales</option>\n"; 
    echo "	    <option name=\"fete\">Pour le temps des Fêtes</option>\n"; 
    echo "	    <option name=\"quebecois\">Québécois</option>\n"; 
    echo "	    <option name=\"famille\">Pour la famille</option>\n"; 
    echo "	  </select><br>\n"; 
    echo "	  Durée:<br><input type='text' class=\"enregIn\" id=\"duree\" name=\"duree\"> min</br>\n"; 
    echo "	  Prix:<br><input type='text' class=\"enregIn\" id=\"prix\" name=\"prix\"> $</br>\n"; 
    echo "	  Image:<br>";
    echo "	  <select class=\"selEnreg\" id=\"image\" name=\"image\">\n"; 
    for($i=1; $i<=10; ++$i)
    {
        echo "	    <option name=\"".$i."\">".$i."</option>\n"; 
    }
    echo "	  </select><br>\n"; 
    echo "	</div>\n"; 
    echo "	<br>\n"; 
    echo "	<input type='hidden' name=\"monAction\" value=\"enregistrer\">\n"; 
    echo "	<input type='hidden' name=\"etatEnreg\" value=\"enreg\">\n"; 
    echo "	<input type='button' class=\"butEnreg2\" value=\"Enregistrer\"\n"; 
    echo "	       onClick='envoyerFormulaire(formEnreg);'>\n"; 
    echo "      </form>\n"; 
    echo "    </div>\n"; 
    echo "\n";
    
}

function envoyerInfoModif()
{
    $film=rechercher();
	if ($film==null)
	    echo "<br><br>Le Film Introuvable.<br><br>";
	else 
    {
        $categories = array("Action", "DrameDrame et Répertoire", "Comédie", "Sience Fiction", "Horreur", "Suspense", "Comédies Musicales", "Pour le temps des Fêtes", "Québécois", "Pour la famille");
        

    echo "      <form name=\"formMAJ\" action=\"gestionFilms.php\" method='post'>\n";
    echo "	<div>\n";  
    echo "	    Numéro:<br><input type='text' class=\"modIn\" id=\"numeroE\" name=\"numeroE\"value=\"".$film->getNumero()."\" readonly></br>\n"; 
    echo "	    Tire:<br><input type='text' class=\"modIn\" id=\"titre\" name=\"titre\" value=\"".$film->getTitre()."\"></br></td>\n"; 
    echo "	  Réalisateur:<br><input type='text' class=\"modIn\" id=\"realisateur\" name=\"realisateur\" value=\"".$film->getRealisateur()."\"></br>";
    echo "	  Catégorie:<br>\n"; 
    echo "	  <select class=\"selModif\" id=\"categorie\" name=\"categorie\">\n"; 
    //pour metter la categorie de $film comme valeur par defaut du tag select
    foreach($categories as $cat)
        {
            if($film->getCategorie() == $cat)
                echo "	    <option name=\"".strtolower($cat)."\" selected>".$cat."</option>\n"; 
            else
                echo "	    <option name=\"".strtolower($cat)."\">".$cat."</option>\n"; 
        }
    echo "	  </select><br><br>\n"; 
    echo "	  Durée:<br><input type='text' class=\"modIn\" id=\"duree\" name=\"duree\" value=\"".$film->getDuree()."\"> min</br>\n"; 
    echo "	  Prix:<br><input type='text' class=\"modIn\" id=\"prix\" name=\"prix\" value=\"".$film->getPrix()."\"> $</br>\n"; 
    echo "	  Image:<br>";
    echo "	  <select class=\"selModif\" id=\"image\" name=\"image\">\n"; 
    for($i=1; $i<=10; ++$i)
    {
        echo "	    <option name=\"".$i."\">".$i."</option>\n"; 
    }
    echo "	  </select><br><br>\n"; 
    echo "	</div>\n"; 
    echo "	<br>\n"; 
    echo "	<input type='hidden' name=\"monAction\" value=\"modifier\">\n"; 
    echo "  <input type='hidden' name=\"etatModif\" value=\"maj\">\n";
    echo "	<input type='button' class=\"butModif2\" value=\"Modifier\"\n"; 
    echo "	       onClick='envoyerFormulaire(formMAJ);'>\n"; 
    echo "      </form>\n";
    }
}

//Controleur
$action=$_POST['monAction'];
switch($action)
{
case "afficher":
    afficherTabFilms();
    break;
case "enregistrer":
    $etatEnreg=$_POST["etatEnreg"];
    if($etatEnreg=="chercherNumIm")
       envoyerInfoEnreg();
    else{//etatEnreg="enreg"
        enregistrer();
    echo "Film ".$_POST['numeroE']." a bien enregistré";
    }
    break;
case "supprimer":
    $aSupp=trim($_POST['numeroS']);
    supprimer($aSupp);
    echo "Film ".$aSupp." a bien supprimé";
    break;
case "modifier":
    $etatModif=$_POST["etatModif"];
    if($etatModif=="rechercher")
        envoyerInfoModif();
    else{//etatModif=="maj" apres recevoir les infos
        $aModif=$_POST["numeroE"];
        supprimer($aModif);
        enregistrer();
    echo "Film ".$aModif." a bien modifié";
    }
    break;
}
echo "<br><br><a href=\"accueilFilm.html\">Retour a la page d'accueil</a>";
?>
